package com.epam.edu.online.stax;

import com.epam.edu.online.model.Gem;
import com.epam.edu.online.model.VisualParams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ParserStAX {
    private static final Logger log = LogManager.getLogger(ParserStAX.class);

    public List<Gem> parseToListByStAX(String path) throws XMLStreamException {
        List<Gem> gems = new ArrayList<>();

        Gem gem = new Gem();
        VisualParams visualParams = new VisualParams();

        File file = new File(path);
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader streamReader = null;
        try {
            streamReader = factory.createXMLStreamReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        while (streamReader.hasNext()) {
            //Move to next event
            streamReader.next();

            //Check if its 'START_ELEMENT'
            if (streamReader.getEventType() == XMLStreamReader.START_ELEMENT) {
                //employee tag - opened
                if (streamReader.getLocalName().equalsIgnoreCase("gem")) {
                    //Create new employee object asap tag is open
                    gem = new Gem();
                    visualParams = new VisualParams();

                    //Read attributes within employee tag
                    if (streamReader.getAttributeCount() > 0) {
                        String id = streamReader.getAttributeValue(null, "id");
                        gem.setId(Integer.valueOf(id));
                    }
                }
                //Read name data
                if (streamReader.getLocalName().equalsIgnoreCase("name")) {
                    gem.setName(streamReader.getElementText());
                }
                //Read title data
                if (streamReader.getLocalName().equalsIgnoreCase("preciousness")) {
                    gem.setPreciousness(Boolean.parseBoolean(streamReader.getElementText()));
                }
                if (streamReader.getLocalName().equalsIgnoreCase("origin")) {
                    gem.setOrigin(streamReader.getElementText());
                }
                if (streamReader.getLocalName().equalsIgnoreCase("carat")) {
                    gem.setCarat(Double.parseDouble(streamReader.getElementText()));
                }
                if (streamReader.getLocalName().equalsIgnoreCase("color")) {
                    visualParams.setColor(streamReader.getElementText());
                }
                if (streamReader.getLocalName().equalsIgnoreCase("facets")) {
                    visualParams.setFacets(Integer.parseInt(streamReader.getElementText()));
                }
                if (streamReader.getLocalName().equalsIgnoreCase("opacity")) {
                    visualParams.setOpacity(Integer.parseInt(streamReader.getElementText()));
                }
                gem.setVisualParams(visualParams);
            }

            //If employee tag is closed then add the employee object to list
            if (streamReader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (streamReader.getLocalName().equalsIgnoreCase("gem")) {
                    gems.add(gem);
                }
            }
        }


        return gems;
    }
}
