package com.epam.edu.online.controller;

import com.epam.edu.online.dom.ParseDOM;
import com.epam.edu.online.model.Gem;
import com.epam.edu.online.sax.ParserSAX;
import com.epam.edu.online.stax.ParserStAX;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLStreamException;
import java.util.Comparator;

public class ControllerImpl implements Controller {
    private static final Logger log = LogManager.getLogger(ControllerImpl.class);

    @Override
    public void parseDOM(String path) {
        log.info("parseDOM");
        long startTime = System.nanoTime();
        new ParseDOM().parseToListByDOM(path)
                .stream()
                .sorted(Comparator.comparingDouble(Gem::getCarat))
                .forEach(log::info);
        printTimeExecuting(startTime);
    }

    @Override
    public void parseSAX(String path) {
        log.info("parseSAX");
        long startTime = System.nanoTime();
        new ParserSAX().parseToListBySAX(path)
                .stream()
                .sorted(Comparator.comparingDouble(Gem::getCarat))
                .forEach(log::info);
        printTimeExecuting(startTime);
    }
    @Override
    public void parseStAX(String path) {
        log.info("parseStAX");
        long startTime = System.nanoTime();
        try {
            new ParserStAX().parseToListByStAX(path)
                    .stream()
                    .sorted(Comparator.comparingDouble(Gem::getCarat))
                    .forEach(log::info);
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } finally {
            printTimeExecuting(startTime);
        }
    }
    private void printTimeExecuting(long startTime) {
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        log.info("Total time  - " + (double) totalTime / 1_000_000_000 + "sec.\n");
    }
}

