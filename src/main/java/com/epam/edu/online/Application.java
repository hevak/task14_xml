package com.epam.edu.online;

import com.epam.edu.online.controller.Controller;
import com.epam.edu.online.controller.ControllerImpl;

public class Application {
    private static final String path = "src/main/resources/gems.xml";
    public static void main(String[] args) {
        Controller controller = new ControllerImpl();
        controller.parseDOM(path);
        controller.parseSAX(path);
        controller.parseStAX(path);
    }
}
