package com.epam.edu.online.dom;

import com.epam.edu.online.model.Gem;
import com.epam.edu.online.model.VisualParams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ParseDOM {
    private static final Logger log = LogManager.getLogger(ParseDOM.class);
    public List<Gem> parseToListByDOM(String path) {
        List<Gem> gems = new ArrayList<>();
        try {
            NodeList nodeList = getNodeList(path);
            for (int temp = 0; temp < nodeList.getLength(); temp++) {
                Node node = nodeList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    gems.add(getGem(element));
                }
            }
        } catch (ParserConfigurationException e) {
            log.error(e.getMessage());
        } catch (SAXException e) {
            log.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return gems;
    }

    private NodeList getNodeList(String path) throws ParserConfigurationException, SAXException, IOException {
        File fXmlFile = new File(path);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        return doc.getElementsByTagName("gem");
    }

    private Gem getGem(Element elem) {
        Gem gem = new Gem();
        gem.setId(Integer.parseInt(elem.getAttribute("id")));
        gem.setName(elem.getElementsByTagName("name").item(0).getTextContent());
        gem.setPreciousness(Boolean.parseBoolean(elem.getElementsByTagName("preciousness").item(0).getTextContent()));
        gem.setOrigin(elem.getElementsByTagName("origin").item(0).getTextContent());
        gem.setCarat(Double.parseDouble(elem.getElementsByTagName("carat").item(0).getTextContent()));
        VisualParams visualParam = new VisualParams();
        visualParam.setColor(elem.getElementsByTagName("color").item(0).getTextContent());
        visualParam.setFacets(Integer.parseInt(elem.getElementsByTagName("facets").item(0).getTextContent()));
        visualParam.setOpacity(Integer.parseInt(elem.getElementsByTagName("opacity").item(0).getTextContent()));
        gem.setVisualParams(visualParam);
        return gem;
    }
}
