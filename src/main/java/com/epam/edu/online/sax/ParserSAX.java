package com.epam.edu.online.sax;

import com.epam.edu.online.model.Gem;
import com.epam.edu.online.model.VisualParams;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;
import java.util.List;

public class ParserSAX {
    private static List<Gem> gems = new ArrayList<>();
    public List<Gem> parseToListBySAX(String path) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            MyHandler handler = new MyHandler();
            saxParser.parse(path, handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gems;
    }

    private static class MyHandler extends DefaultHandler {
        String lastElementName;
        Integer id = 0;
        String name;
        Boolean preciousness;
        String origin;
        Double carat;
        String color;
        Integer facets;
        Integer opacity;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equals("gem")) {
                id = Integer.parseInt(attributes.getValue("id"));
            }
            lastElementName = qName;
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String information = new String(ch, start, length);
            information = information.replace("\n", "").trim();
            if (!information.isEmpty()) {
                if (lastElementName.equals("name")) {
                    name = information;
                }
                if (lastElementName.equals("preciousness")) {
                    preciousness = Boolean.parseBoolean(information);
                }
                if (lastElementName.equals("origin")) {
                    origin = information;
                }
                if (lastElementName.equals("carat")) {
                    carat = Double.parseDouble(information);
                }
                if (lastElementName.equals("color")) {
                    color = information;
                }
                if (lastElementName.equals("facets")) {
                    facets = Integer.parseInt(information);
                }
                if (lastElementName.equals("opacity")) {
                    opacity = Integer.parseInt(information);
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            if ((name != null && !name.isEmpty())
                    && (preciousness != null)
                    && (origin != null && !origin.isEmpty())
                    && (carat != null)
                    && (color != null && !color.isEmpty())
                    && (facets != null)
                    && (opacity != null)) {
                gems.add(new Gem(id, name, preciousness, origin, new VisualParams(color, facets, opacity), carat));
                id = null;
                name = null;
                preciousness = null;
                origin = null;
                carat = null;
                color = null;
                facets = null;
                opacity = null;
            }
        }

    }
}
